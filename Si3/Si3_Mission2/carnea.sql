-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `Collection`;
CREATE TABLE `Collection` (
  `NumColl` int(15) NOT NULL,
  `DateL` date DEFAULT NULL,
  `NomColl` varchar(30) DEFAULT NULL,
  `Harmnonie` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`NumColl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Collection` (`NumColl`, `DateL`, `NomColl`, `Harmnonie`) VALUES
(1,	'2005-04-01',	'Marée Haute',	'Harmonie'),
(2,	'2005-04-15',	'Soleil',	'Jaune'),
(3,	'2011-10-18',	'Vent Pourpre',	'Harmonie R');

DROP TABLE IF EXISTS `Consultation`;
CREATE TABLE `Consultation` (
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  `prenomMedecin` varchar(20) NOT NULL,
  PRIMARY KEY (`numeroProprio`,`numeroAnimal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `Consultation` (`numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `prenomMedecin`) VALUES
(1,	1,	'2013-09-15',	'Occulaire',	'Gabrielle');

DROP TABLE IF EXISTS `Produit`;
CREATE TABLE `Produit` (
  `Refproduit` varchar(30) NOT NULL,
  `Design` varchar(30) DEFAULT NULL,
  `Couleur` varchar(30) DEFAULT NULL,
  `Dim` varchar(30) DEFAULT NULL,
  `PrixHT` double DEFAULT NULL,
  `NumColl` int(30) DEFAULT NULL,
  PRIMARY KEY (`Refproduit`),
  KEY `NumColl` (`NumColl`),
  CONSTRAINT `Produit_ibfk_4` FOREIGN KEY (`NumColl`) REFERENCES `Collection` (`NumColl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Produit` (`Refproduit`, `Design`, `Couleur`, `Dim`, `PrixHT`, `NumColl`) VALUES
('A12',	'Chaise Longue',	'Marine',	'120*90',	90,	1),
('A14',	'Serviette de Bain',	'Orangé',	'130*80',	65,	2),
('A15',	'Coussin',	'Paille',	'30*30',	12,	2);

-- 2017-09-21 09:27:23
