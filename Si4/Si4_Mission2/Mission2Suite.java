import java.util.Scanner;


public class Mission2Suite {


	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Initialisation
		// double prixHT;
		double tauxTVA=0.2;
		// double quantiteAchetee;
		double prixTTC;
		// String nomArticle="";

		// Traitement
		Scanner sc=new Scanner(System.in);
		// Nom
		System.out.println("Saisissez le nom de l'article :");
		String nomArticle=sc.nextLine(); //Affectation
		System.out.println("Le nom de l'article est :"+nomArticle);
		// Quantite
		System.out.println("Saisissez la quantite d'articles achetes :");
		double quantiteAchetee=sc.nextDouble(); //Affectation
		System.out.println("Quantite achetee = "+quantiteAchetee);
		// PrixHT
		System.out.println("Saisissez le prix d'un article achete :");
		double prixHT=sc.nextDouble(); //Affectation
		System.out.println("Prix d'un article ="+prixHT);
		// PrixTTC
		prixTTC=(prixHT*(1+tauxTVA))*quantiteAchetee; //Affectation
		
		// Comparatif
		if (quantiteAchetee>100){
			prixTTC=(prixTTC)*0.9; //Affectation 2
			System.out.println("Le prix TTC de "+quantiteAchetee+" articles "+nomArticle+ " a  " +prixHT+" ,avec une TVA a  20% et une reduction de 10% est de ="+prixTTC+" euros");

		}			
		else System.out.println("Vous n'avez pas le droit a  une reduction mais : Le prix TTC de "+quantiteAchetee+" articles "+nomArticle+ " a  " +prixHT+" ,avec une TVA a  20% est de ="+prixTTC+" euros");
		
		}

	}

/*
Algo :



Debut :
Variables : prixHT, tauxTVA, quantiteAchetee, prixTTC, nomArticle
tauxTVA = 0.2
Traitement :
Saisir nonmArticle
Afficher("Le nom de l'article est :")
Saisir quantiteAchetee
Afficher("Quantite achetee = "+quantiteAchetee)
Saisir prixHT
prixTTC=(prixHT*(1+tauxTVA))*quantiteAchetee
Si quantiteAchetee>100
	prixTTC=(prixTTC)*0.9
	Afficher("Le prix TTC de "+quantiteAchetee+" articles "+nomArticle+ " a  " +prixHT+" ,avec une TVA a  20% et une reduction de 10% est de ="+prixTTC+" euros")
Sinon 
	Afficher("Vous n'avez pas le droit a  une reduction mais : Le prix TTC de "+quantiteAchetee+" articles "+nomArticle+ " a  " +prixHT+" ,avec une TVA a  20% est de ="+prixTTC+" euros")
Sortie :
Afficher("Le prix TTC de "+quantiteAchetee+" articles "+nomArticle+ " a  " +prixHT+" ,avec une TVA a  20% est de ="+prixTTC+" euros")
*/
