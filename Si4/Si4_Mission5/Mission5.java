import java.util.Scanner;

public class miss5
{
	public static void main(String[] args)
	{
		int total = 0;
		String output = "";
		for (int i=1; i<=3; i++)
		{
			System.out.print("Enter number " + i + ": ");
			Scanner scanner = new Scanner(System.in);
			while (!scanner.hasNextInt()){
				scanner.next();
			}
			int input = scanner.nextInt();
			total += input;
			output = output +" " + input + " +";

		}
		output = output.substring(1);
		output = output.substring(0, output.length()-2) + " = " + total;
		System.out.println(output);
		System.out.println("L'addition est : " + total + " et la somme est : " + output);
	}
}


/*Algo Mission5Q1
Variables : total (entier), output (chaine), i (entier), scanner	

Debut : 
total=0
output=""
	Pour (i=1;i<=3;i=i+1)
		Tant que (!scanner.hasNextInt)		//Scanner n'a pas d'entier
			Faire scanner
		Saisir input
		total = total+input
		output = output+"" +input+ " +"
		FinTantque
	outuput=      //Je ne sais pas convertir en algo ces lignes	
Fin			    
 */