import java.util.Scanner;
public class Mission5Q2{
	private static Scanner sc;

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		int nb=0;
		String res ="";
		int rep = 0;
		int choix;
		for (int indice=1; indice<=5;indice++) {
			System.out.println("Entrer un entier");
			nb = sc.nextInt();
			System.out.println("Entrer 1 => addition, et 2 => soustraction.");
			choix = sc.nextInt();
			if (choix == 1) {
				if(indice == 5){
					res = res + nb;
					rep = rep + nb;
				}
				else{
					res = res + nb + " + ";
					rep = rep + nb;
				}
			}
			else {
				res = res + " - " + nb;
				rep = rep - nb;
			}
		}
		System.out.println(res + " = " + rep);
	}
}

/*Algo Mission5Q2

 Variables : nb, rep, choix, indice entier, res

  Debut :
       nb <= 0
       rep <= 0
		Pour indice de 0 a 5 inclus pas 1
			Faire
                Saisir nb
                Saisir choix
                Si choix = 1
                    Alors
                        Si indice = 5
                            res = res + nb
                            rep = rep + nb
                        Sinon
                            res = res + nb + " + "
                            rep = rep + nb
                        FinSi
                Sinon
                    res = res + " - " + nb
                    rep = rep - nb
                FinSi
        FinPour
    Afficher res + " = " + rep
    Fin
 */